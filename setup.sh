# disable the restart dialogue and install several packages
sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo apt-get update
sudo apt install wget git python3 python3-venv build-essential net-tools awscli -y

# install CUDA (from https://developer.nvidia.com/cuda-downloads)
wget https://developer.download.nvidia.com/compute/cuda/12.0.0/local_installers/cuda_12.0.0_525.60.13_linux.run
sudo sh cuda_12.0.0_525.60.13_linux.run --silent

# install git-lfs
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
sudo -u ubuntu git lfs install --skip-smudge

# download the SD model v2.1 and move it to the SD model directory
sudo -u ubuntu git clone --depth 1 https://huggingface.co/stabilityai/stable-diffusion-2-1-base
cd stable-diffusion-2-1-base/
sudo -u ubuntu git lfs pull --include "v2-1_512-ema-pruned.ckpt"
sudo -u ubuntu git lfs install --force
cd ..
mv stable-diffusion-2-1-base/v2-1_512-ema-pruned.ckpt stable-diffusion-webui/models/Stable-diffusion/
rm -rf stable-diffusion-2-1-base/

# download the corresponding config file and move it also to the model directory (make sure the name matches the model name)
wget https://raw.githubusercontent.com/Stability-AI/stablediffusion/main/configs/stable-diffusion/v2-inference.yaml
cp v2-inference.yaml stable-diffusion-webui/models/Stable-diffusion/v2-1_512-ema-pruned.yaml

# Add Extentions
cd stable-diffusion-webui
git clone https://github.com/Mikubill/sd-webui-controlnet.git extensions/sd-webui-controlnet
git clone https://github.com/hnmr293/sd-webui-llul.git extensions/sd-webui-llul
git clone https://github.com/hnmr293/posex.git extensions/posex
git clone https://github.com/DominikDoom/a1111-sd-webui-tagcomplete.git extensions/a1111-sd-webui-tagcomplete
git clone https://github.com/hnmr293/sd-webui-cutoff.git extensions/sd-webui-cutoff
git clone https://github.com/adieyal/sd-dynamic-prompts.git extensions/sd-dynamic-prompts
git clone https://github.com/opparco/stable-diffusion-webui-two-shot.git extensions/stable-diffusion-webui-two-shot
git clone https://github.com/nonnonstop/sd-webui-3d-open-pose-editor.git extensions/sd-webui-3d-open-pose-editor
# Download model data
cd models/Stable-diffusion
curl -o ../VAE/anything-v4.0.vae.pt -L https://huggingface.co/andite/anything-v4.0/resolve/main/anything-v4.0.vae.pt &
curl -O -L https://huggingface.co/andite/anything-v4.0/resolve/main/anything-v4.5-pruned.safetensors &
curl -O -L https://huggingface.co/waifu-diffusion/wd-1-5-beta/resolve/main/checkpoints/wd15-beta1-fp16.safetensors &
curl -O -L https://huggingface.co/waifu-diffusion/wd-1-5-beta/resolve/main/checkpoints/wd15-beta1-fp16.yaml &
curl -o ../Lora/animeLineartMangaLike_v30MangaLike.safetensors -L https://civitai.com/api/download/models/28907
# Download Embdding
cd ../../embeddings
curl -O -L https://huggingface.co/datasets/gsdf/EasyNegative/resolve/main/EasyNegative.safetensors &
curl -O -L https://huggingface.co/waifu-diffusion/wd-1-5-beta/resolve/main/embeddings/wdbadprompt.pt &
curl -O -L https://huggingface.co/waifu-diffusion/wd-1-5-beta/resolve/main/embeddings/wdgoodprompt.bin
# Download Controlnet data
cd ../extensions/sd-webui-controlnet/models
curl -O -L https://huggingface.co/thibaud/controlnet-sd21/resolve/main/canny-sd21-safe.safetensors &
curl -O -L https://huggingface.co/thibaud/controlnet-sd21/resolve/main/openpose-sd21-safe.safetensors &
curl -O -L https://huggingface.co/thibaud/controlnet-sd21/resolve/main/scribble-sd21-safe.safetensors &
curl -O -L https://huggingface.co/webui/ControlNet-modules-safetensors/resolve/main/control_scribble-fp16.safetensors &
curl -O -L https://huggingface.co/webui/ControlNet-modules-safetensors/resolve/main/control_canny-fp16.safetensors
wait


# change ownership of the web UI so that a regular user can start the server
sudo chown -R ubuntu:ubuntu stable-diffusion-webui/

# start the server as user 'ubuntu'
# sudo -u ubuntu nohup bash stable-diffusion-webui/webui.sh --listen > log.txt


